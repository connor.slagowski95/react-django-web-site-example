from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from .models import Students
from .serializers import *

# Create your views here.

@api_view(['GET', 'POST'])

def students_list(request):
    if request.method == 'GET':
        data = Students.objects.all()

        serializers = StudentSerializer(data, context={'request': request}, many=True)

        return Response(serializers.data)

    elif request.method == 'POST':
        serializers = StudentSerializer(data = request.data)
        if serializers.is_valid():
            serializers.save()
            return Response(status=status.HTTP_201_CREATED)

        return Response(serializers.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT', 'DELETE'])
def students_detail(request, pk):
    try:
        student = Students.objects.get(pk=pk)
    except Students.DoesNotExist:
        return Response(status = status.HTTP_400_BAD_REQUEST)

    if request.method == 'PUT':
        serializer = StudentSerializer(student, data=request.data, context={'request:': request})
        if serializer.is_valid():
            serializer.save()
            return Response(status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        student.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)
